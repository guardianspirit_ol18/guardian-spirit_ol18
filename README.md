# guardian-spirit_ol18
A project is for OpenLab2018@UEC. It demonstrates Computer Vision technology. It indicate a guardian spirit of the person.


## master branch
タグをつけるブランチにしましょう．
とりあえずリポジトリの運用方針はGit-flowに習う感じで．
でも，そこまでこだわってないです．

## 結合作業
細かい作業は，releaseでやる．
ブランチにrelease/**.**があります．
タスクが完了したらこのブランチにマージしていきます．
コンフリクト，各タスクなどの結合，実行テストなどはここでやる感じでお願いします．


## 個人の作業

ディレクトリはフォークしてください．
Githubだと管理権限で個人のレポジトリにプッシュできないそうです．

ローカル環境では専用のブランチを切りましょう．
ex). develop/montage_character
必ずdevelop/****というふうにしましょう

参考までに  
+ develop/montage_character:人物の背景にキャラクターを合成する
+ develop/GUI:しゅごキャラ！を生成する際のエセ心理テストＧＵＩ
+ develop/analize_human:人物のポーズから特徴を分析
+ develop/analize_face:顔から特徴を分析
+ develop/analize:画像から人物を分析する
+ develop/make_character:キャラクターを生成する

などなど，ブランチで作業している内容がわかる名前であれば何でもいいです．

