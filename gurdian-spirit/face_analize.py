#-----------------------------------------------------
# 入力画像から性別，年齢，表情を推測する
# 入力画像の指定方法や，パラメータの返し方は考慮が必要
#-----------------------------------------------------

import json
import os

from tf_openpose import common

def analize(image_path):

	# 引数：入力画像のパス
	# 戻り値：推測された性別，年齢，表情
	#-----------------------------------------------------
	# Microsoft Face API
	#-----------------------------------------------------
	import requests
	import http.client,urllib

	key = '5527dd4655d34522977ab94041e276c4'

	header = {
	'Ocp-Apim-Subscription-Key': key,
	'Content-Type': 'application/octet-stream',
	}

	image_data = open(image_path, "rb")
	body = image_data.read()
	image_data.close()

	params = urllib.parse.urlencode({
	'returnFaceId': 'false',
	'returnFaceLandmarks': 'false',
	'returnFaceAttributes': 'age,gender,emotion',
	#'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
	})

	conn = http.client.HTTPSConnection('eastasia.api.cognitive.microsoft.com')
	conn.request("POST", "/face/v1.0/detect?%s" % params, body, header)
	response = conn.getresponse()
	data = json.loads(response.read())

	gender = data[0]['faceAttributes']['gender']
	age = data[0]['faceAttributes']['age']
	emotion = data[0]['faceAttributes']['emotion']

	conn.close()
	return gender,age,emotion

	#-----------------------------------------------------
	# IBM Watson Visual VisualRecognition（使わない）
	#-----------------------------------------------------

	#from watson_developer_cloud import VisualRecognitionV3

	#visual_recognition = VisualRecognitionV3(
	#	'2018-03-19',
	#	api_key='cb0da175edce2c509251bff75582ff9e8e7e0bf5')

	#with open(image_path, 'rb') as images_file:
	#	faces = visual_recognition.detect_faces(images_file)
	#print(json.dumps(faces, indent=2))


if __name__ == '__main__':

	# human_region_extractで生成された前景画像を入力画像とする
	path = './fdg_test.png'
	gender,age,emotion = analize(path)

	print('gender : {0}'.format(gender))
	print("age : {0}".format(age))
	print("emotion : {0}".format(emotion))
