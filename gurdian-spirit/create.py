from part import *

feature = {
    'body': 1,
    'face': 1,
    'hair': 1,
    'eyebrow': 1,
    'eye': 1,
    'nose': 1,
    'mouth': 1,
    'R': 200,
    'G': 100,
    'B': 100
}
c = character(feature)
c.body_face_merge()
c.show()
