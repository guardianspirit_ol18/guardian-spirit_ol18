# -*- coding: utf-8 -*-
# 　フォントジェネレーター：https://aratama.github.io/kemonogen/
# ------------------------------説明---------------------------
# １　スタートページ
# 　　　　　↓　Enterキー
# ２　エセ心理テストページ１
# ３　エセ心理テストページ２
# ４　解析ページ
# ５　結果表示ページ
#          ↓　Enterキー
# １　スタートページ（不安定？）
# 　
# 終了時：Escキー
# ------------------------------------------------------------
import tkinter
from tkinter import ttk
from PIL import ImageTk, Image, ImageFilter
import random
from threading import Timer
import subprocess
import cv2
from tf_openpose import common
import pygame
from time import sleep

# モジュールの読み込み
import webcam_caputur
from montage_character import run


# ====================================================================================
# クラス定義部
# ====================================================================================

# ------------------------------------------------------------------------------------
# トップページ
# ------------------------------------------------------------------------------------
class StartPage(tkinter.Frame):
    def __init__(self, parent, controller):
        tkinter.Frame.__init__(self, parent)
        self.controller = controller

        self.img = Image.open("./images/logo/logo2.png")
        self.img = ImageTk.PhotoImage(self.img)
        imageLabel = tkinter.Label(self, image=self.img)

        ExLabelFont = ("Helevetice", 20)
        ExLabel = tkinter.Label(self, text="１　クイズに２問答えてください \n\n ２　守護霊を呼び出します", \
                                font=ExLabelFont)

        # パーツの配置
        imageLabel.grid(row=0, column=0, sticky=tkinter.S)
        ExLabel.grid(row=1, column=0, sticky=tkinter.N)
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=2)
        self.columnconfigure(0, weight=1)

        # イベントの登録
        se = pygame.mixer.Sound("./music/start.wav")
        self.bind("<Return>", lambda x: se.play())
        self.bind("<Return>", lambda x: self.Start(4), "+")
        self.focus_set()

    def Start(self, count):
        if count > 0:
            count = count - 1
            t = Timer(1, self.Start, args=[count])
            t.start()

        elif count == 0:
            self.controller.show_frame("QuestionPage")


# ------------------------------------------------------------------------------------
# エセ心理テストページ１
# ------------------------------------------------------------------------------------
class QuestionPage(tkinter.Frame):

    def __init__(self, parent, controller):
        tkinter.Frame.__init__(self, parent)
        self.controller = controller

        titleLabelFont = ("Helevetice", 32)
        titleLabel = tkinter.Label(self, text="Ｑ：以下の状況を想定して自由にポーズをとってください", \
                                   font=titleLabelFont)

        ImgName = "./images/pose_image/img" + str(self.controller.get_image_num(1)) + ".jpg"
        self.img = Image.open(ImgName)
        # 画像のサイズ変更
        self.img.thumbnail((600, 500), Image.ANTIALIAS)
        self.img = ImageTk.PhotoImage(self.img)
        imageLabel = tkinter.Label(self, image=self.img)

        self.buff = tkinter.StringVar()
        self.buff.set(' ')
        TimerFont = ("Helevetice", 32, "bold")
        TimerLabel = tkinter.Label(self, textvariable=self.buff, font=TimerFont)

        # パーツの配置
        titleLabel.grid(row=0, column=0, sticky=tkinter.S)
        imageLabel.grid(row=1, column=0, sticky=tkinter.N)
        TimerLabel.grid(row=2, column=0, sticky=tkinter.N)
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)
        self.columnconfigure(0, weight=1)

        # イベントの登録
        # global webcam
        # webcam = webcam_caputur.WebCamCapture(_timer=0,_file="capture1.jpg")
        # self.cam = cv2.VideoCapture(0)
        self.filename = "capture1.jpg"
        self.bind("<Map>", lambda x: self.countdown(6))
        self.focus_set()

    def countdown(self, count):
        if count == 6:
            se = pygame.mixer.Sound("./music/countdown.wav")
            se.play()

        if count > 0:
            count = count - 1
            self.buff.set(count)
            t = Timer(1, self.countdown, args=[count])
            t.start()

        elif count == 0:
            # webcam_caputurの処理が優先されてしまうため，
            # webcam_caputur.py上でのカウントダウンは行わない
            # result, image = webcam.capture()
            se = pygame.mixer.Sound("./music/Shutter.wav")
            cam = cv2.VideoCapture(0)
            ret_val, image = cam.read()
            se.play()
            cv2.imwrite(self.filename, image)
            # 処理がはやすぎるためちょっとお休み
            sleep(1)
            self.controller.show_frame("QuestionPage2")


# ------------------------------------------------------------------------------------
# エセ心理テストページ２
# ------------------------------------------------------------------------------------
class QuestionPage2(tkinter.Frame):

    def __init__(self, parent, controller):
        tkinter.Frame.__init__(self, parent)
        self.controller = controller

        spaceLabel1 = [tkinter.Label(self, text="") for column in range(5)]
        titleLabelFont = ("Helevetice", 32)
        titleLabel = tkinter.Label(self, text="Ｑ：以下の状況を想定して自由にポーズをとってください", \
                                   font=titleLabelFont)

        ImgName = "./images/pose_image/img" + str(self.controller.get_image_num(2)) + ".jpg"
        self.img = Image.open(ImgName)
        # 画像のサイズ変更
        self.img.thumbnail((600, 500), Image.ANTIALIAS)
        self.img = ImageTk.PhotoImage(self.img)
        imageLabel = tkinter.Label(self, image=self.img)

        self.buff = tkinter.StringVar()
        self.buff.set(' ')
        TimerFont = ("Helevetice", 32, "bold")
        TimerLabel = tkinter.Label(self, textvariable=self.buff, font=TimerFont)

        # パーツの配置
        titleLabel.grid(row=0, column=0, sticky=tkinter.S)
        imageLabel.grid(row=1, column=0, sticky=tkinter.N)
        TimerLabel.grid(row=2, column=0, sticky=tkinter.N)
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)
        self.columnconfigure(0, weight=1)

        # イベントの登録
        self.filename = "capture2.jpg"
        self.bind("<Map>", lambda x: self.countdown(6))
        self.focus_set()

    def countdown(self, count):
        if count == 6:
            se = pygame.mixer.Sound("./music/countdown.wav")
            se.play()

        if count > 0:
            count = count - 1
            self.buff.set(count)
            t = Timer(1, self.countdown, args=[count])
            t.start()

        elif count == 0:
            # result, image = webcam.capture()
            se = pygame.mixer.Sound("./music/Shutter.wav")
            cam = cv2.VideoCapture(0)
            ret_val, image = cam.read()
            se.play()
            sleep(1)
            se = pygame.mixer.Sound("./music/wait.wav")
            se.play()
            cv2.imwrite(self.filename, image)
            self.controller.show_frame("AnalizePage")


# ------------------------------------------------------------------------------------
# 解析中ページ
# ------------------------------------------------------------------------------------
class AnalizePage(tkinter.Frame):
    def __init__(self, parent, controller):
        tkinter.Frame.__init__(self, parent)
        self.controller = controller

        # 進行状況バーの値（適当）
        self.bar = 0
        self.pb = ttk.Progressbar(
            self,
            length=200,
            mode='determinate')
        self.pb.configure(maximum=20, value=self.bar)

        self.img1 = Image.open("./images/logo/kaiseki.png")
        self.img1.thumbnail((1920, 300), Image.ANTIALIAS)
        self.img1 = ImageTk.PhotoImage(self.img1)
        imageLabel1 = tkinter.Label(self, image=self.img1)

        self.buff = tkinter.StringVar()
        self.buff.set(' 神があなたの守護霊を作るとき　')
        self.dic = {}
        self.dic2 = {}
        self.FLAG = False
        LabelFont = ("Helevetice", 20)
        TextLabel = tkinter.Label(self, textvariable=self.buff, font=LabelFont)

        self.img2 = Image.open("./images/god.png")
        self.img2 = ImageTk.PhotoImage(self.img2)
        imageLabel2 = tkinter.Label(self, image=self.img2)

        # パーツの配置
        imageLabel1.grid(row=0, column=0, sticky=tkinter.S)
        self.pb.grid(row=1, column=0, sticky=tkinter.N)
        TextLabel.grid(row=2, column=0, sticky=tkinter.S)
        imageLabel2.grid(row=3, column=0, sticky=tkinter.N)
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.rowconfigure(2, weight=1)
        self.rowconfigure(3, weight=2)
        self.columnconfigure(0, weight=1)

        # イベントの登録
        # Expose だと複数回コールされてしまう
        self.bind("<Map>", lambda x: self.countdown())
        self.focus_set()

    # 解析作業
    def app(self):
        path = 'test_parameters.py'
        subprocess.call("python %s" % path)

        char_img_raw = cv2.imread('./char_test.png', cv2.IMREAD_UNCHANGED)
        fdg_mask = cv2.imread('./mask_capture1.jpg', cv2.IMREAD_GRAYSCALE)
        capture = cv2.imread('./capture1.jpg', cv2.IMREAD_COLOR)
        result_pil = run(character_raw=char_img_raw, mask_fdg_raw=fdg_mask, capture_img=capture)

        result_pil.save("mnt.jpg", quality=95)
        import ast
        file = open('consts.py', 'r', encoding='utf-8')
        self.dic1 = ({"com1": "神があなたの守護霊を作るとき", "com2": "みなさんこんにちは", "com3": "今日はあなたの守護霊を作りたいと思います。"})
        self.dic2 = ast.literal_eval(file.read())
        self.dic3 = ({"com1": "こんなもんかな～", "com2": "あとは仕上げに色味を少々", "com3": "なんか気持ち悪…まあいっか"})
        self.keys1 = [k for k, v in self.dic1.items()]
        self.keys2 = [k for k, v in self.dic2.items()]
        self.keys3 = [k for k, v in self.dic3.items()]
        self.FLAG = True

    def countdown(self):
        if self.bar == 0:
            self.app()
            pygame.mixer.music.stop()
            pygame.mixer.music.load("./music/cooking.mp3")
            pygame.mixer.music.set_volume(0.3)
            pygame.mixer.music.play(1)

        if self.bar < 20 and self.FLAG == True:
            self.pb.configure(value=self.bar)

            if self.bar == 10:
                se = pygame.mixer.Sound("./music/go_to_result.wav")
                se.play()

            if self.bar <= 3:
                s = str(self.dic1[self.keys1[self.bar - 1]])
                self.buff.set(s)

            if self.bar > 3 and self.bar <= 15:
                s = str(self.keys2[self.bar - 4]) + "は" + str(self.dic2[self.keys2[self.bar - 4]]) + "くらい"
                self.buff.set(s)

            if self.bar > 15 and self.bar <= 18:
                s = str(self.dic3[self.keys3[self.bar - 16]])
                self.buff.set(s)

            self.bar += 1
            t = Timer(1, self.countdown)
            t.start()

        elif self.bar >= 20:
            self.controller.show_frame("ResultPage")


# ------------------------------------------------------------------------------------
# 結果表示ページ
# ------------------------------------------------------------------------------------
class ResultPage(tkinter.Frame):
    def __init__(self, parent, controller):
        tkinter.Frame.__init__(self, parent)
        self.controller = controller

        self.img1 = Image.open("./images/logo/result.png")
        self.img1.thumbnail((1920, 200), Image.ANTIALIAS)
        self.img1 = ImageTk.PhotoImage(self.img1)
        titleLabel = tkinter.Label(self, image=self.img1)

        # 合成結果を表示するラベルの設定
        self.imageLabel = tkinter.Label(self, image="")

        # パーツの配置
        titleLabel.grid(row=0, column=0, sticky=tkinter.N)
        self.imageLabel.grid(row=1, column=0, sticky=tkinter.N)
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=3)
        self.columnconfigure(0, weight=1)

        # イベントの登録
        self.bind("<Expose>", lambda x: self.disp_result())
        self.bind("<Return>", lambda x: controller.show_frame("StartPage"))
        self.focus_set()

    # 合成結果の表示
    def disp_result(self):
        pygame.mixer.music.stop()
        pygame.mixer.music.load("./music/bgm.mp3")
        pygame.mixer.music.set_volume(0.3)
        pygame.mixer.music.play(-1)

        se = pygame.mixer.Sound("./music/result.wav")
        se.play()
        ImgName = "./mnt.jpg"
        self.img2 = Image.open(ImgName)

        self.img2 = ImageTk.PhotoImage(self.img2)
        self.imageLabel.configure(image=self.img2)


class main(tkinter.Tk):

    def __init__(self, *args, **kwargs):
        tkinter.Tk.__init__(self, *args, **kwargs)

        container = tkinter.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        # BGMの設定
        pygame.init()
        pygame.mixer.music.load("./music/bgm.mp3")
        pygame.mixer.music.set_volume(0.3)
        pygame.mixer.music.play(-1)

        # 画像の枚数を増やした場合は変更
        self.image_num = random.sample(range(1, 6), 2)

        self.bind("<Escape>", lambda x: self.exit())
        self.frames = {}
        for F in (StartPage, QuestionPage, QuestionPage2, AnalizePage, ResultPage):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

        # 一番最初に表示するページ
        self.show_frame("StartPage")

    # 指定したフレームを表示
    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.grid(row=0, column=0, sticky="nsew")
        frame.tkraise()

    # テストに使用する２枚の画像を取得（重複を避けるため）
    def get_image_num(self, i):
        return self.image_num[i - 1]

    # プログラムを終了する
    def exit(self):
        pygame.mixer.music.stop()
        self.quit()


# ====================================================================================
# 本体処理
# ====================================================================================
if __name__ == "__main__":
    main = main()
    # 画面のサイズを指定
    main.title("しゅごきゃらっ")
    main.geometry("1920x1080")
    main.mainloop()
