##
# Webカメラを起動してオプションで指定した時間後にキャプチャを保存する
# taopipi
##

import argparse
import logging
import time

import cv2
from threading import Timer, Thread, Event

logger = logging.getLogger('webcam-caputure')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class RepeatalTimer():

    def __init__(self, t, hFunction):
        # self.args = args
        self.t = t
        self.hFunction = hFunction
        self.thread = Timer(self.t, self.handle_function)

    def handle_function(self):
        self.hFunction()
        self.thread = Timer(self.t, self.handle_function)
        self.thread.start()

    def start(self):
        self.thread.start()

    def cancel(self):
        self.thread.cancel()


class WebCamCapture(object):
    fps_time = 0

    def __init__(self, _cam_num=0, _timer=5, _file="test.jpg"):
        """
        :param cam_num: int type default 0
        :param resolution: tuple int type (w, h) default (640, 480)
        :param timre: int type countdown timer
        :param file: str type file name default test.jpg
        """
        self.file = _file
        self.limit = _timer
        self.cam = cv2.VideoCapture(_cam_num)

    def countdown_handler(self):
        """
        カウントダウンハンドラ
        :return:
        """
        print("Timer:{}".format(self.limit))
        if self.limit <= 0:
            pass
        else:
            self.limit -= 1

    def capture(self):
        """
        Webカメラを起動して撮影する
        :param debug: bool debugable or not default False
        :return: result bool, capture_img: opencv BGR numpy array
        """
        logger.debug('cam read+')
        ret_val, image = self.cam.read()
        cv2.imwrite(self.file, image)
        return True, image

    def capture_with_thread(self, debug=False):
        """
        Webカメラを起動して、時間経過後撮影する

        :param debug: bool debugable or not default False
        :return: result bool, capture_img: opencv BGR numpy array
        """
        logger.debug('thread setting+')
        thread = RepeatalTimer(1, self.countdown_handler)

        logger.debug('cam read+')
        ret_val, image = self.cam.read()

        logger.info('cam image=%dx%d' % (image.shape[1], image.shape[0]))

        thread.start()
        while True:
            ret_val, image = cam.read()
            logger.debug('caputur+')
            logger.debug("CountDown:{}".format(self.limit))
            if self.limit <= 0 and not self.limit == -1:
                # タイムアウト時にキャプチャ
                cv2.imwrite(self.file, image)
                if debug:
                    cv2.destroyAllWindows()
                break
            if debug:
                # デバッグするときに画面に出力
                logger.debug('show+')
                cv2.putText(image,
                            "FPS: %f" % (1.0 / (time.time() - self.fps_time)),
                            (10, 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                            (0, 255, 0), 2)
                cv2.putText(image,
                            "%d" % (self.limit),
                            (0, image.shape[0]), cv2.FONT_HERSHEY_SIMPLEX, 5,
                            (255, 255, 255), 2)
                fps_time = time.time()
                cv2.imshow('tf-pose-estimation result', image)
            if cv2.waitKey(1) & 0xff == ord('q'):
                # qを押した時にキャプチャ
                cv2.imwrite(self.file, image)
                if debug:
                    cv2.destroyAllWindows()
                break
        logger.debug('finished+')
        thread.cancel()
        return True, image


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='tf-pose-estimation realtime webcam')
    parser.add_argument('--camera', type=int, default=0)
    parser.add_argument('--timer', type=int, default=5, help='captur timer')
    parser.add_argument('--file', type=str, default='test.jpg', help='output file name')
    args = parser.parse_args()

    obj_capt = WebCamCapture(_cam_num=args.camera, _timer=args.timer, _file=args.file)
    result, image = obj_capt.capture()
    # result, image = obj_capt.capture(debug=True)
