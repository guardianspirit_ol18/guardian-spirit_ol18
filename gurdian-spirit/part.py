#!/usr/bin/python3
# Filename: part.py

import sys
# sys.path.remove("/opt/ros/kinetic/lib/python2.7/dist-packages")
import cv2
import numpy as np

class part:

    def __init__(self, name, feature):
        self.img = cv2.imread(feature, cv2.IMREAD_UNCHANGED)
        self.name = name
        self.feature = feature

    def adjustment(self, rows_, cols_):
        size = ((int)(rows_), (int)(cols_))
        self.img = cv2.resize(self.img, size, interpolation=cv2.INTER_AREA)

    # cv2.imwrite(self.feature + "__.png", self.img)

    def show(self):
        cv2.imshow(self.name, self.img)
        print(self.name, " ", self.feature)

    def find_middle_point(self):
        middle_point = []
        mouth_list = []
        for i in range(self.img.shape[0]):
            length_ = 0
            for j in range(self.img.shape[1]):
                if self.img[i, j, 3] != 0:
                    length_ += 1
            if length_ > 10:
                mouth_list.append(i)

        mouth_point_row = (int)((mouth_list[len(mouth_list) - 1] + mouth_list[0]) / 2)

        start_col = []
        end_col = []
        for j in range(1, self.img.shape[1] - 1):
            if self.img[mouth_point_row, j, 3] != 0 and self.img[mouth_point_row, j - 1, 3] == 0:
                start_col.append(j)
            if self.img[mouth_point_row, j, 3] != 0 and self.img[mouth_point_row, j + 1, 3] == 0:
                end_col.append(j)

        if len(start_col) == 0:
            start_col.append(0)
            end_col.append(self.img.shape[1])

        max_length_i = 0
        max_length = 0

        if len(start_col) <= len(end_col):
            num = len(start_col)
        else:
            num = len(end_col)

        for i in range(num):
            if end_col[i] - start_col[i] > max_length:
                max_length = end_col[i] - start_col[i]
                max_length_i = i

        print(max_length_i)
        mouth_point_col = (int)((end_col[max_length_i] + start_col[max_length_i]) / 2)

        middle_point = [mouth_point_row, mouth_point_col]
        return middle_point

    def get_width(self, flag):
        if flag == 'face':
            row = (int)(self.img.shape[0] * 0.4)
        if flag == 'hair':
            row = (int)(self.img.shape[0] * 0.6)

        width = 0
        start = 0
        end = 0
        flag = 0
        for j in range(1, self.img.shape[1] - 1):
            if self.img[row, j, 3] != 0 and self.img[row, j - 1, 3] != 0 and flag == 0:
                start = j
                flag = 1
            if self.img[row, j, 3] != 0 and self.img[row, j + 1, 3] == 0:
                end = j

        width = end - start
        if width < self.img.shape[1] * 0.3:
            width = self.img.shape[1]

        return width


class face:

    # fixed size 185 * 175
    def __init__(self, feature):
        self.feature = feature
        self.eyebrow = part("eyebrow", "../parts/mayu{0}.png".format(feature['eyebrow']))
        self.eye = part("eye", "../parts/eye{0}.png".format(feature['eye']))
        self.nose = part("nose", "../parts/n{0}.png".format(feature['nose']))
        self.mouth = part("mouth", "../parts/m{0}.png".format(feature['mouth']))
        self.hair = part("hair", "../parts/h{0}.png".format(feature['hair']))
        self.faze = part("faze", "../parts/face{0}.png".format(feature['face']))

        self.chin_point = []
        self.mouth_point = []
        self.mouth_point_ = []
        self.nose_point = []
        self.nose_point_ = []
        self.face_img = np.zeros([10, 10, 4], np.uint8)

    def adjustment(self):
        self.faze.adjustment(185, 175)
        faze_width = self.faze.get_width("face")
        hair_width = self.hair.get_width("hair")
        hair_rate = hair_width / faze_width
        self.hair.adjustment(self.hair.img.shape[1] / hair_rate, self.hair.img.shape[0] / hair_rate)
        self.eyebrow.adjustment(140, 20)
        self.eye.adjustment(140, 30)
        self.nose.adjustment(45, 45)
        self.mouth.adjustment(60, 20)
        self.find_chin()

    def find_chin(self):
        chin_list = []
        chin_point_col = 0
        rows_ = self.faze.img.shape[0] - 1
        cols_ = self.faze.img.shape[1]
        for i in range(rows_):
            for j in range(cols_):
                if len(chin_list) != 51:
                    if self.faze.img[rows_ - i, j, 0] != 0 and self.faze.img[rows_ - i, j, 1] != 0 and self.faze.img[
                        rows_ - i, j, 2] != 0:
                        chin_list.append([rows_ - i, j])
                else:
                    break

        for i in range(len(chin_list)):
            chin_point_col += chin_list[i][1]
        chin_point_col /= 51
        self.chin_point = [chin_list[0][0], (int)(chin_point_col)]

    def parts_merge(self):
        self.adjustment()

        self.mouth_point_ = self.mouth.find_middle_point()
        self.mouth_point = [self.chin_point[0] - 15, self.chin_point[1]]
        self.nose_point_ = self.nose.find_middle_point()
        self.nose_point = [self.mouth_point[0] - 30, self.chin_point[1]]

        print(self.nose_point_)
        for i in range(self.eyebrow.img.shape[0]):
            for j in range(self.eyebrow.img.shape[1]):
                if self.eyebrow.img[i, j, 3] != 0:
                    self.faze.img[i + 75, j + 20, 0] = self.eyebrow.img[i, j, 0]
                    self.faze.img[i + 75, j + 20, 1] = self.eyebrow.img[i, j, 1]
                    self.faze.img[i + 75, j + 20, 2] = self.eyebrow.img[i, j, 2]

        for i in range(self.eye.img.shape[0]):
            for j in range(self.eye.img.shape[1]):
                if self.eye.img[i, j, 3] != 0:
                    self.faze.img[i + 80, j + 25, 0] = self.eye.img[i, j, 0]
                    self.faze.img[i + 80, j + 25, 1] = self.eye.img[i, j, 1]
                    self.faze.img[i + 80, j + 25, 2] = self.eye.img[i, j, 2]

        offset_x = self.nose_point[0] - self.nose_point_[0]
        offset_y = self.nose_point[1] - self.nose_point_[1]

        for i in range(self.nose.img.shape[0]):
            for j in range(self.nose.img.shape[1]):
                if self.nose.img[i, j, 3] != 0:
                    self.faze.img[i + offset_x, j + offset_y, 0] = self.nose.img[i, j, 0]
                    self.faze.img[i + offset_x, j + offset_y, 1] = self.nose.img[i, j, 1]
                    self.faze.img[i + offset_x, j + offset_y, 2] = self.nose.img[i, j, 2]

        offset_x = self.mouth_point[0] - self.mouth_point_[0]
        offset_y = self.mouth_point[1] - self.mouth_point_[1]

        for i in range(self.mouth.img.shape[0]):
            for j in range(self.mouth.img.shape[1]):
                if self.mouth.img[i, j, 3] != 0:
                    self.faze.img[i + offset_x, j + offset_y, 0] = self.mouth.img[i, j, 0]
                    self.faze.img[i + offset_x, j + offset_y, 1] = self.mouth.img[i, j, 1]
                    self.faze.img[i + offset_x, j + offset_y, 2] = self.mouth.img[i, j, 2]

        hair_point_row = (int)(self.hair.img.shape[0] / 2)
        face_point_row = (int)(self.faze.img.shape[0] / 3)

        hair_point_col_s = 0
        hair_point_col_e = 0
        face_point_col_s = 0
        face_point_col_e = 0

        flag = 0
        for j in range(1, self.hair.img.shape[1] - 1):
            if flag == 0 and self.hair.img[hair_point_row, j, 3] != 0 and self.hair.img[hair_point_row, j - 1, 3] == 0:
                flag = 1
                hair_point_col_s = j
            if self.hair.img[hair_point_row, j, 3] != 0 and self.hair.img[hair_point_row, j + 1, 3] == 0:
                hair_point_col_e = j

        hair_point_col = (int)((hair_point_col_e + hair_point_col_s) / 2)
        flag = 0
        for j in range(1, self.faze.img.shape[1] - 1):
            if flag == 0 and self.faze.img[face_point_row, j, 3] != 0 and self.faze.img[face_point_row, j - 1, 3] == 0:
                flag = 1
                face_point_col_s = j
            if self.faze.img[face_point_row, j, 3] != 0 and self.faze.img[face_point_row, j + 1, 3] == 0:
                face_point_col_e = j

        face_point_col = (int)((face_point_col_e + face_point_col_s) / 2)

        hair_point = [hair_point_row, hair_point_col]
        face_point = [face_point_row, face_point_col]

        face_im_row = (int)(self.hair.img.shape[0] / 2 + 2 * self.faze.img.shape[0] / 3)

        if self.hair.img.shape[1] > hair_point[1]:
            face_im_col = 2 * (self.hair.img.shape[1] - hair_point[1])
        else:
            face_im_col = 2 * hair_point[1]

        if face_im_col < self.faze.img.shape[1]:
            face_im_col = self.faze.img.shape[1]

        size = face_im_col, face_im_row
        self.face_img = cv2.resize(self.face_img, size, interpolation=cv2.INTER_AREA)

        if self.faze.feature != "../parts/face3.png":

            offset_x = hair_point[0] - face_point[0] - 1
            offset_y = (int)(self.face_img.shape[1] / 2) - face_point[1]

            for i in range(self.faze.img.shape[0]):
                for j in range(self.faze.img.shape[1]):
                    if self.faze.img[i, j, 3] != 0:
                        self.face_img[i + offset_x, j + offset_y, 0] = self.faze.img[i, j, 0]
                        self.face_img[i + offset_x, j + offset_y, 1] = self.faze.img[i, j, 1]
                        self.face_img[i + offset_x, j + offset_y, 2] = self.faze.img[i, j, 2]
                        self.face_img[i + offset_x, j + offset_y, 3] = self.faze.img[i, j, 3]

            offset_y = (int)(self.face_img.shape[1] / 2) - hair_point[1]
            for i in range(self.hair.img.shape[0]):
                for j in range(self.hair.img.shape[1]):
                    if self.hair.img[i, j, 3] != 0:
                        self.face_img[i, j + offset_y, 0] = (int)(self.feature['B'])
                        self.face_img[i, j + offset_y, 1] = (int)(self.feature['G'])
                        self.face_img[i, j + offset_y, 2] = (int)(self.feature['R'])
                        self.face_img[i, j + offset_y, 3] = 255

            self.faze.img = self.face_img.copy()


class character:

    def __init__(self, feature):

        self.feature = feature
        self.faze_ = face(feature)
        self.body_ = part("body", "../parts/b{0}.png".format(feature['body']))
        self.character_im = np.zeros([10, 10, 4], np.uint8)

    def body_face_merge(self):
        self.faze_.parts_merge()
        self.faze_.find_chin()
        chin_point_col = 0
        neck_line_row = 0

        if self.body_.img.shape[0] < self.body_.img.shape[1] * 2:
            for i in range(self.body_.img.shape[0]):
                num = 0
                for j in range((int)(self.body_.img.shape[1] * 0.3), (int)(self.body_.img.shape[1] * 0.6)):
                    if self.body_.img[i, j, 3]:
                        num += 1
                if num == 10:
                    neck_line_row = i
                    break

        col_1 = (int)(self.body_.img.shape[1] * 0.3)
        col_2 = 0
        for j in range((int)(self.body_.img.shape[1] * 0.3), (int)(self.body_.img.shape[1] * 0.6)):
            if self.body_.img[neck_line_row, j, 3] != 0 and self.body_.img[neck_line_row, j + 1, 3] == 0:
                col_1 = j
                break
        for j in range((int)(self.body_.img.shape[1] * 0.3), (int)(self.body_.img.shape[1] * 0.6)):
            if self.body_.img[neck_line_row, j, 3] == 0 and self.body_.img[neck_line_row, j + 1, 3] != 0:
                col_2 = j

        if self.body_.img.shape[0] < self.body_.img.shape[1] * 2:
            chin_point_body = [neck_line_row + 20, (int)((col_1 + col_2) / 2)]
        else:
            chin_point_body = [12, 136]

        character_im_cols = self.body_.img.shape[1]
        character_im_rows = 0
        if self.body_.img.shape[0] - neck_line_row + self.faze_.faze.img.shape[0] > self.body_.img.shape[0]:
            character_im_rows = self.body_.img.shape[0] - neck_line_row + self.faze_.faze.img.shape[0]
        else:
            character_im_rows = self.body_.img.shape[0]
        size = character_im_cols, character_im_rows
        self.character_im = cv2.resize(self.character_im, size, interpolation=cv2.INTER_AREA)

        if self.faze_.faze.img.shape[0] - neck_line_row <= 0:
            for i in range(self.body_.img.shape[0]):
                for j in range(self.body_.img.shape[1]):
                    # copy body part
                    self.character_im[i, j, 0] = self.body_.img[i, j, 0]
                    self.character_im[i, j, 1] = self.body_.img[i, j, 1]
                    self.character_im[i, j, 2] = self.body_.img[i, j, 2]

            offset_x = chin_point_body[0] - self.faze_.chin_point[0]
            offset_y = chin_point_body[1] - self.faze_.chin_point[1]

            for i in range(self.faze_.faze.img.shape[0]):
                for j in range(self.faze_.faze.img.shape[1]):
                    # copy face
                    if self.faze_.faze.img[i, j, 3] != 0:
                        if j + offset_y < self.character_im.shape[1] and i + offset_x < self.character_im.shape[0]:
                            self.character_im[i + offset_x, j + offset_y, 0] = self.faze_.faze.img[i, j, 0]
                            self.character_im[i + offset_x, j + offset_y, 1] = self.faze_.faze.img[i, j, 1]
                            self.character_im[i + offset_x, j + offset_y, 2] = self.faze_.faze.img[i, j, 2]

        else:
            offset = self.faze_.faze.img.shape[0] - neck_line_row
            for i in range(self.body_.img.shape[0]):
                for j in range(self.body_.img.shape[1]):
                    self.character_im[i + offset, j, 0] = self.body_.img[i, j, 0]
                    self.character_im[i + offset, j, 1] = self.body_.img[i, j, 1]
                    self.character_im[i + offset, j, 2] = self.body_.img[i, j, 2]

            chin_point_body[0] = chin_point_body[0] + self.faze_.chin_point[0] - neck_line_row
            offset_x = chin_point_body[0] - self.faze_.chin_point[0]
            offset_y = chin_point_body[1] - self.faze_.chin_point[1]
            for i in range(self.faze_.faze.img.shape[0]):
                for j in range(self.faze_.faze.img.shape[1]):
                    if self.faze_.faze.img[i, j, 3] != 0:
                        if j + offset_y < self.character_im.shape[1]:
                            self.character_im[i + offset_x, j + offset_y, 0] = self.faze_.faze.img[i, j, 0]
                            self.character_im[i + offset_x, j + offset_y, 1] = self.faze_.faze.img[i, j, 1]
                            self.character_im[i + offset_x, j + offset_y, 2] = self.faze_.faze.img[i, j, 2]

    def show(self):
        tmp = cv2.cvtColor(self.character_im, cv2.COLOR_BGRA2BGR)
        cv2.imwrite("char_test.png", tmp)
