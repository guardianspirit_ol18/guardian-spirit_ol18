﻿import itertools
import logging
import math
from collections import namedtuple

import cv2
import numpy as np
import tensorflow as tf
from scipy.ndimage import maximum_filter, gaussian_filter

import copy

from tf_openpose import common
from tf_openpose.common import CocoPairsNetwork, CocoPairs, CocoPart

import json
import os
import io

import requests
import http.client, urllib

import pickle


class GET_ETC:
    def get_BGR_value(img1, img2):  # RGB値の取得(人物領域のRGB値)
        # 変数宣言
        bgr_1 = [0.0, 0.0, 0.0]  # B・G・R
        count = 0
        img1_h, img1_w = img1.shape[:2]
        # 色取得
        for y in range(img1_h):
            for x in range(img1_w):
                color = img1[y, x]
                if (color[2] == 0 and color[1] == 0 and color[0] == 0) or (
                        color[2] == 255 and color[1] == 255 and color[0] == 255):
                    continue
                bgr_1 += img1[y, x]
                count += 1
        # 色の正規化(色の合計/前景のピクセル数)
        bgr_1[0] = int(float(bgr_1[0]) / float(count))
        bgr_1[1] = int(float(bgr_1[1]) / float(count))
        bgr_1[2] = int(float(bgr_1[2]) / float(count))

        # 変数宣言
        bgr_2 = [0.0, 0.0, 0.0]  # B・G・R
        count = 0
        img2_h, img2_w = img2.shape[:2]
        # 色取得
        for y in range(img2_h):
            for x in range(img2_w):
                color = img2[y, x]
                if (color[2] == 0 and color[1] == 0 and color[0] == 0) or (
                        color[2] == 255 and color[1] == 255 and color[0] == 255):
                    continue
                bgr_2 += img2[y, x]
                count += 1
        # 色の正規化(色の合計/前景のピクセル数)
        bgr_2[0] = bgr_2[0] / count
        bgr_2[1] = bgr_2[1] / count
        bgr_2[2] = bgr_2[2] / count

        bgr = [0, 0, 0]

        bgr[0] = (bgr_1[0] + bgr_2[0]) / 2
        bgr[1] = (bgr_1[1] + bgr_2[1]) / 2
        bgr[2] = (bgr_1[2] + bgr_2[2]) / 2

        return bgr

    def get_face_info(proxies_dict):
        key = "5527dd4655d34522977ab94041e276c4"
        image_path_1 = open("capture1.jpg", "rb").read()
        image_path_2 = open("capture2.jpg", "rb").read()

        emotion_recognition_rul = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect"
        #        proxy_dict={"http":"http://KinoshitaYosuke:Kinoshita0124@proxy.uec.ac.jp:8080/","https":"http://KinoshitaYosuke:Kinoshita0124@proxy.uec.ac.jp:8080/"}
        header = {'Ocp-Apim-Subscription-Key': key, "Content-Type": "application/octet-stream"}
        params = urllib.parse.urlencode(
            {'returnFaceId': 'false', 'returnFaceLandmarks': 'false', 'returnFaceAttributes': 'age,gender,emotion'})

        response_1 = requests.post(emotion_recognition_rul, headers=header, params=params, data=image_path_1,
                                   proxies=proxies_dict)
        analysis_1 = response_1.json()
        analysis_1

        response_2 = requests.post(emotion_recognition_rul, headers=header, params=params, data=image_path_2,
                                   proxies=proxies_dict)
        analysis_2 = response_2.json()
        analysis_2

        check = [0, 0]

        if len(analysis_1) != 0:
            check[0] = 1
            gender_1 = analysis_1[0]['faceAttributes']['gender']
            age_1 = analysis_1[0]['faceAttributes']['age']
            emotion_1 = analysis_1[0]['faceAttributes']['emotion']
        if len(analysis_2) != 0:
            check[1] = 1
            gender_2 = analysis_2[0]['faceAttributes']['gender']
            age_2 = analysis_2[0]['faceAttributes']['age']
            emotion_2 = analysis_2[0]['faceAttributes']['emotion']
        response_1.close()
        response_2.close()

        # 正規化(性別：男なら1，女なら10　年齢：値/10　表情：値*10)
        if check[0] == 1:
            gender_value_1 = 0
            age_value_1 = int(age_1 / 10)
            emotion_value_1 = {}
            emotion_value_1[0] = int(emotion_1['anger'] * 10)
            emotion_value_1[1] = int(emotion_1['contempt'] * 10)
            emotion_value_1[2] = int(emotion_1['disgust'] * 10)
            emotion_value_1[3] = int(emotion_1['fear'] * 10)
            emotion_value_1[4] = int(emotion_1['happiness'] * 10)
            emotion_value_1[5] = int(emotion_1['neutral'] * 10)
            emotion_value_1[6] = int(emotion_1['sadness'] * 10)
            emotion_value_1[7] = int(emotion_1['surprise'] * 10)
            if gender_1 == 'male':
                gender_value_1 = 1
            else:
                gender_value_1 = 10
        if check[1] == 1:
            gender_value_2 = 0
            age_value_2 = int(age_2 / 10)
            emotion_value_2 = {}
            emotion_value_2[0] = int(emotion_2['anger'] * 10)
            emotion_value_2[1] = int(emotion_2['contempt'] * 10)
            emotion_value_2[2] = int(emotion_2['disgust'] * 10)
            emotion_value_2[3] = int(emotion_2['fear'] * 10)
            emotion_value_2[4] = int(emotion_2['happiness'] * 10)
            emotion_value_2[5] = int(emotion_2['neutral'] * 10)
            emotion_value_2[6] = int(emotion_2['sadness'] * 10)
            emotion_value_2[7] = int(emotion_2['surprise'] * 10)
            if gender_2 == 'male':
                gender_value_2 = 1
            else:
                gender_value_2 = 10

        age_value = 0
        gender_value = 0
        emotion_value = {}

        if check[0] == 1:
            if check[1] == 1:
                gender_value = (gender_value_1 + gender_value_2) / 2
                age_value = (age_value_1 + age_value_2) / 2
                for i in range(8):
                    emotion_value[i] = (emotion_value_1[i] + emotion_value_2[i]) / 2
            else:
                gender_value = gender_value_1
                age_value = age_value_1
                for i in range(8):
                    emotion_value[i] = emotion_value_1[i]
        elif check[1] == 1:
            gender_value = gender_value_2
            age_value = age_value_2
            for i in range(8):
                emotion_value[i] = emotion_value_2[i]
        else:
            emotion_value = {}
            for i in range(8):
                emotion_value[i] = 0
            return 0, 0, emotion_value

        return gender_value, age_value, emotion_value

    def get_physique(centers_1, centers_2):  # 体格の抽出(上半身の縦横比)
        check = [0, 0]

        # 右肩幅と左肩幅の計算
        if centers_1[1][0] != 10000 and centers_1[2][0] != 10000 and centers_1[5][0] != 10000 and centers_1[8][
            0] != 10000 and centers_1[11][0] != 10000:
            check[0] = 1
            shoulder_center = centers_1[1]
            shoulder_dis_x1 = abs(centers_1[2][0] - centers_1[1][0])
            shoulder_dis_x2 = abs(centers_1[5][0] - centers_1[1][0])
            shoulder_dis_y1 = abs(centers_1[2][1] - centers_1[1][1])
            shoulder_dis_y2 = abs(centers_1[5][1] - centers_1[1][1])
            shoulder_dis_1 = np.sqrt(pow(shoulder_dis_x1, 2) + pow(shoulder_dis_y1, 2))
            shoulder_dis_2 = np.sqrt(pow(shoulder_dis_x2, 2) + pow(shoulder_dis_y1, 2))
            shoulder_dis = shoulder_dis_1 + shoulder_dis_2
            # 腰幅の計算
            waist_x = abs(centers_1[8][0] - centers_1[11][0])
            waist_y = abs(centers_1[8][1] - centers_1[11][1])
            waist_dis = np.sqrt(pow(waist_x, 2) + pow(waist_y, 2))
            waist_center = [(centers_1[8][0] + centers_1[11][0]) / 2, (centers_1[8][1] + centers_1[11][1]) / 2]
            # 上半身の長さの計算
            upper_dis = np.sqrt(
                pow(shoulder_center[0] - waist_center[0], 2) + pow(shoulder_center[1] - waist_center[1], 2))
            # 上半身の縦横比の計算：上半身の長さ / ((肩幅＋腰幅)/2)
            physique_aspect_1 = float(upper_dis) / ((shoulder_dis + waist_dis) / 2)

        if centers_2[1][0] != 10000 and centers_2[2][0] != 10000 and centers_2[5][0] != 10000 and centers_2[8][
            0] != 10000 and centers_2[11][0] != 10000:
            check[1] = 1
            shoulder_center = centers_2[1]
            shoulder_dis_x1 = abs(centers_2[2][0] - centers_2[1][0])
            shoulder_dis_x2 = abs(centers_2[5][0] - centers_2[1][0])
            shoulder_dis_y1 = abs(centers_2[2][1] - centers_2[1][1])
            shoulder_dis_y2 = abs(centers_2[5][1] - centers_2[1][1])
            shoulder_dis_1 = np.sqrt(pow(shoulder_dis_x1, 2) + pow(shoulder_dis_y1, 2))
            shoulder_dis_2 = np.sqrt(pow(shoulder_dis_x2, 2) + pow(shoulder_dis_y1, 2))
            shoulder_dis = shoulder_dis_1 + shoulder_dis_2
            # 腰幅の計算
            waist_x = abs(centers_2[8][0] - centers_2[11][0])
            waist_y = abs(centers_2[8][1] - centers_2[11][1])
            waist_dis = np.sqrt(pow(waist_x, 2) + pow(waist_y, 2))
            waist_center = [(centers_2[8][0] + centers_2[11][0]) / 2, (centers_2[8][1] + centers_2[11][1]) / 2]
            # 上半身の長さの計算
            upper_dis = np.sqrt(
                pow(shoulder_center[0] - waist_center[0], 2) + pow(shoulder_center[1] - waist_center[1], 2))
            # 上半身の縦横比の計算：上半身の長さ / ((肩幅＋腰幅)/2)
            physique_aspect_2 = float(upper_dis) / ((shoulder_dis + waist_dis) / 2)

        if check[0] == 1:
            if check[1] == 1:
                physique_aspect = (physique_aspect_1 + physique_aspect_2) / 2
            else:
                physique_aspect = physique_aspect_1
        elif check[1] == 1:
            physique_aspect = physique_aspect_2
        else:
            return -1

        print(physique_aspect)
        # 正規化(今のところ×4)
        return int(physique_aspect * 4)

    def get_movement(humans_1, humans_2):  # 関節移動量の取得

        movement_x = 0
        movement_y = 0
        for i in humans_1:
            if (humans_1[i][0] == 10000 or humans_2[i][0] == 10000):
                continue
            movement_x += humans_2[i][0] - humans_1[i][0]
            movement_y += humans_2[i][1] - humans_1[i][1]

        movement = float(np.sqrt(pow(movement_x, 2) + pow(movement_y, 2)))

        if (movement > 1000):
            return 10
        else:
            return int(movement / 100)

    def get_centers(humans, image):  # 各関節座標の抽出
        image_h, image_w = image.shape[:2]
        centers = {}
        for human in humans:
            for i in range(common.CocoPart.Background.value):
                if i not in human.body_parts.keys():
                    centers[i] = (10000, 10000)
                    continue

                body_part = human.body_parts[i]
                center = (int(body_part.x * image_w + 0.5), int(body_part.y * image_h + 0.5))
                centers[i] = center

        return centers
