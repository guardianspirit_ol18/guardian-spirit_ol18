import argparse
import logging
import time
import ast

from tf_openpose import common
import cv2
import numpy as np
from tf_openpose.estimator import TfPoseEstimator
from tf_openpose.networks import get_graph_path, model_wh
from get_parameters import GET_ETC

import copy

from tf_openpose.lifting.prob_model import Prob3dPose
from tf_openpose.lifting.draw import plot_pose

from threading import Timer, Thread, Event

from decide_parts import DECIDE_PARTS
from set_parts import SET_PARTS
from human_region_extract import make_human_region, extract_foreground_humans
from webcam_caputur import *

from parser import *

from part import *

logger = logging.getLogger('TfPoseEstimator')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='tf-pose-estimation realtime webcam')
    parser.add_argument('--resolution', type=str, default='432x368', help='network input resolution. default=432x368')
    parser.add_argument('--model', type=str, default='mobilenet_thin', help='cmu / mobilenet_thin')
    parser.add_argument('--scales', type=str, default='[None]', help='for multiple scales, eg. [1.0, (1.1, 0.05)]')
    parser.add_argument('--camera', type=int, default=0)
    parser.add_argument('--timer', type=int, default=1, help='captur timer')
    parser.add_argument('--file', type=str, default='test.png', help='output file name')
    args = parser.parse_args()

    scales = ast.literal_eval(args.scales)

    w, h = model_wh(args.resolution)
    e = TfPoseEstimator(get_graph_path(args.model), target_size=(w, h))
    # 必要な情報：前景画像2枚，プロキシのホストとポート番号(http://username:password@proxy_host:port_number/)
    # 画像読み込み(仮)
    image = {}
    image[0] = common.read_imgfile("./capture1.jpg", None, None)
    image[1] = common.read_imgfile("./capture2.jpg", None, None)
    #    obj_capt = WebCamCapture(_cam_num=args.camera, _timer=args.timer, _file=args.file)
    #    result, image[0] = obj_capt.capture(debug=True, something = parser.parse_args())
    #    obj_capt = WebCamCapture(_cam_num=args.camera, _timer=args.timer, _file=args.file)
    #    result, image[1] = obj_capt.capture(debug=True, something = parser.parse_args())

    proxies_dict = {"http": "http://KinoshitaYosuke:Kinoshita0124@proxy.uec.ac.jp:8080/",
                    "https": "http://KinoshitaYosuke:Kinoshita0124@proxy.uec.ac.jp:8080/"}

    # ボーン推定
    humans = {}
    with common.Timer(logger, "一枚目のボーン推定"):
        humans[0] = e.inference(image[0], scales=scales)
    with common.Timer(logger, "二枚目のボーン推定"):
        humans[1] = e.inference(image[1], scales=scales)

    # 前景画像抽出処理
    rects = {}
    fgd_masks = {}
    pre_masks = {}
    final_mask = {}
    fg_img = {}
    bg_img = {}
    with common.Timer(logger, "人物領域抽出"):
        for i in range(2):
            rects[i], fgd_masks[i], pre_masks[i] = make_human_region(image[i], humans[i])
    with common.Timer(logger, "前景抽出"):
        for i in range(2):
            final_mask[i], fg_img[i], bg_img[i] = extract_foreground_humans(image[i], fgd_masks[i], pre_masks[i])


    def combert2bgra(img, mask):
        b, g, r = cv2.split(img)
        alpha = mask.astype(np.uint8) * 255
        bgra = cv2.merge((b, g, r, alpha))
        return bgra


    cv2.imwrite("mask_capture1.jpg", final_mask[0].astype(np.uint8) * 255)
    cv2.imwrite("mask_capture2.jpg", final_mask[1].astype(np.uint8) * 255)
    cv2.imwrite("human_extract_1.png", combert2bgra(fg_img[0], final_mask[0]))
    cv2.imwrite("human_extract_2.png", combert2bgra(fg_img[1], final_mask[1]))

    #   cv2.destroyAllWindows()

    proxy_dict = {"http": "http://username:password@proxy_host:port_number/",
                  "https": "http://username:password@proxy_host:port_number/"}

    parts, dic = SET_PARTS.set_parts(image[0], image[1], humans[0], humans[1], proxies_dict)

    file = open('consts.py', 'w', encoding='utf-8')
    file.write(str(dic))

    print(parts)
    # キャラ生成
    c = character(parts)
    c.body_face_merge()
    c.show()
