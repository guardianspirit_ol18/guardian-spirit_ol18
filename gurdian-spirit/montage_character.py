import argparse
import logging
import cv2
import numpy as np
from PIL import Image, ImageFilter
from skimage.morphology import skeletonize, medial_axis
import math

logger = logging.getLogger('human_region_extract')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def BGR_RGB(img):
    """
    BGR <-> RGBに変換
    :param img:
    :return:
    """
    return img[:, :, ::-1].copy()


def make_bgr_a(bgr_raw):
    gray = cv2.cvtColor(bgr_raw, cv2.COLOR_BGR2GRAY)
    if gray[0, 0] != 0:
        tmp = gray - 255
        gray = tmp * -1
    ret, thresh = cv2.threshold(gray, 5, 255, cv2.THRESH_BINARY)
    return thresh, bgr_raw


def botom_blur_mask(alpha):
    """
    半透明ブラー処理をかける
    :param charcter_img:
    :return: 加工画像
    """
    base_mask = np.full(alpha.shape[:2], 255, np.uint8)
    start_row = int(alpha.shape[0] * 0.90)
    base_mask[start_row:] = 0
    base_mask = cv2.blur(base_mask, (1, 71)) / 255
    char_mask = cv2.blur(alpha, ksize=(30, 30))
    blur_mask = (char_mask * base_mask).astype(np.uint8)
    return blur_mask


def generate_gradation_top2bottom(img, grd_start, grd_width):
    up = 255
    down = 0
    h, w = img.shape[:2]
    if h < grd_start - 1 + grd_width:
        ofset = h - grd_start - grd_width
        grd_width = grd_start + ofset
        bt_width = 0
    else:
        bt_width = h - grd_start - grd_width
    white = int(grd_width * 0.8)
    inv_white = grd_width - white
    x = np.linspace(white, -inv_white, grd_width, endpoint=True).astype(int)
    grd = np.ones(grd_width) + np.tanh(0.5 * 0.06 * x)
    grd = grd * 0.5 * 255
    grd = grd.astype(np.uint8)
    head = np.full(grd_start, up, dtype=np.uint8)
    bottom = np.full(bt_width, down, dtype=np.uint8)
    gradation = np.concatenate((head, grd, bottom))

    return np.tile(gradation, (w, 1)).T


def make_like_split(char_img_raw):
    if char_img_raw.shape[-1] == 4:
        alpha = char_img_raw[:, :, 3].copy()
        char_bgr = char_img_raw[:, :, :3].copy()
    else:
        alpha, char_bgr = make_bgr_a(char_img_raw)

    alpha = np.where(alpha > 200, 200, alpha)
    char_rgb = BGR_RGB(char_bgr)
    grd = generate_gradation_top2bottom(alpha, int(alpha.shape[0] * 0.4), int(alpha.shape[0] * 0.6))
    tmp = np.multiply(alpha, grd / 255).astype(np.uint8)
    mask = Image.fromarray(tmp, 'L')
    rgb_pil = Image.fromarray(char_rgb, "RGB")
    mask_blur = mask.filter(ImageFilter.GaussianBlur(20))
    hoge = np.asarray(mask_blur, dtype=np.uint8)
    tmp = np.multiply(hoge, alpha / 255).astype(np.uint8)
    mask_blur = Image.fromarray(tmp, 'L')
    rgb_pil.putalpha(mask_blur)
    return rgb_pil


def calculate_position_size(bdg_mask):
    cv2.rectangle(bdg_mask, (0, 0), (bdg_mask.shape[1] - 1, bdg_mask.shape[0] - 1), 0, 1)
    # perform skeletonization
    skel, dist = medial_axis(bdg_mask, return_distance=True)
    index = np.where(dist == dist.max())
    x_max = int(index[1][0])
    y_max = int(index[0][0])
    pt4opencv = (x_max, y_max)
    radian = int(dist[y_max, x_max])

    return pt4opencv, radian


def resize_charcter(char_pil, radian):
    char_w = char_pil.width
    char_h = char_pil.height
    aspect_ratio = char_w / char_h
    theta = math.atan(aspect_ratio)
    cos_theta = math.cos(theta)
    sin_theta = math.sin(theta)
    char_m_h = int(2 * radian * cos_theta)
    char_m_w = int(2 * radian * sin_theta)
    r_char_pil = char_pil.resize((char_m_w, char_m_h), Image.BICUBIC)
    return r_char_pil


def run(character_raw, mask_fdg_raw, capture_img):
    """
    合成あぷり
    :param character_raw:
    :param mask_fdg_raw: マスク画像 GRAY
    :param capture_img: キャプチャ画像 BGR
    :return: PIL type 合成結果
    """
    if type(character_raw) is not np.ndarray:
        character_raw = np.asarray(character_raw, dtype=np.uint8)
    rgb_pil = make_like_split(character_raw)
    res, bdg_mask = cv2.threshold(mask_fdg_raw, 10, 1, cv2.THRESH_BINARY_INV)
    pt4opencv, radian = calculate_position_size(bdg_mask)
    r_char_pil = resize_charcter(rgb_pil, radian)
    r_char_width = r_char_pil.width
    r_char_height = r_char_pil.height
    char_center_pt = (int(pt4opencv[0] - r_char_width / 2), int(pt4opencv[1] - r_char_height / 2))

    if type(capture_img) is np.ndarray:
        capture_img = cv2.cvtColor(capture_img, cv2.COLOR_BGR2RGB)
        montage_img = Image.fromarray(capture_img)
    else:
        montage_img = capture_img.copy()
    # result = Image.alpha_composite(layer1, )
    montage_img.paste(r_char_pil, char_center_pt, r_char_pil)
    # from PIL import ImageDraw
    # draw = ImageDraw.Draw(montage_img)
    # draw.rectangle((int(pt4opencv[0] - r_char_width / 2),
    #               int(pt4opencv[1] - r_char_height / 2),
    #               int(pt4opencv[0] + r_char_width / 2),
    #               int(pt4opencv[1] + r_char_height / 2)), outline=(255, 0, 0))
    # draw.ellipse((int(pt4opencv[0] - radian),
    #               int(pt4opencv[1] - radian),
    #               int(pt4opencv[0] + radian),
    #               int(pt4opencv[1] + radian)), outline=(0, 255, 0))

    return montage_img


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='montage_charcter')
    parser.add_argument('--character', type=str, default='./images/haster.png')
    parser.add_argument('--capture', type=str, default='./test_tao3.jpg')
    parser.add_argument('--mask', type=str, default='./mask_test_tao3.jpg')

    args = parser.parse_args()
    char_img_raw = cv2.imread(args.character, cv2.IMREAD_UNCHANGED)
    fdg_mask = cv2.imread(args.mask, cv2.IMREAD_GRAYSCALE)
    capture = cv2.imread(args.capture, cv2.IMREAD_COLOR)

    result_pil = run(character_raw=char_img_raw, mask_fdg_raw=fdg_mask, capture_img=capture)
    result_pil.show(title="合成結果")
    import os

    fname_char = os.path.splitext(os.path.basename(args.character))[0]
    fname_capt = os.path.splitext(os.path.basename(args.capture))[0]
    result_pil.save("mnt_{0}_{1}.jpg".format(fname_capt, fname_char), quality=95)
