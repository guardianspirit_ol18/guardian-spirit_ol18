import argparse
import logging
import time
import ast

from tf_openpose import common
import cv2
import numpy as np
from tf_openpose.estimator import TfPoseEstimator
from tf_openpose.networks import get_graph_path, model_wh
from get_parameters import GET_ETC

import copy

from tf_openpose.lifting.prob_model import Prob3dPose
from tf_openpose.lifting.draw import plot_pose

from decide_parts import DECIDE_PARTS


# logger = logging.getLogger('TfPoseEstimator')
# logger.setLevel(logging.DEBUG)
# ch = logging.StreamHandler()
# ch.setLevel(logging.DEBUG)
# formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
# ch.setFormatter(formatter)
# logger.addHandler(ch)

class SET_PARTS:
    def set_parts(image_1, image_2, human_1, human_2, proxies_dict):
        #        parser = argparse.ArgumentParser(description='tf-pose-estimation run')
        #        parser.add_argument('--image', type=str, default='../../images/p1.jpg')
        #        parser.add_argument('--resolution', type=str, default='432x368', help='network input resolution. default=432x368')
        #        parser.add_argument('--model', type=str, default='mobilenet_thin', help='cmu / mobilenet_thin')
        #        parser.add_argument('--scales', type=str, default='[None]', help='for multiple scales, eg. [1.0, (1.1, 0.05)]')
        #        args = parser.parse_args()
        #        scales = ast.literal_eval(args.scales)

        #        w, h = model_wh(args.resolution)

        # 特徴量情報(各関節移動量・rgb値・年齢・表情・性別)
        movement = 0
        bgr = [0, 0, 0]
        centers_1 = {}
        centers_2 = {}
        physique = 0

        #        e = TfPoseEstimator(get_graph_path(args.model), target_size=(w, h))

        # estimate human poses from a single image !
        #        t = time.time()
        #        humans = {}
        #        humans[0] = e.inference(image_1, scales=scales)
        #        humans[1] = e.inference(image_2, scales=scales)
        #        elapsed = time.time() - t

        # 色抽出
        bgr = GET_ETC.get_BGR_value(image_1, image_2)

        # 性別・年齢・感情抽出
        # human_region_extractで生成された前景画像を入力画像とする
        gender, age, emotion = GET_ETC.get_face_info(proxies_dict)
        print(gender)
        print(age)
        print(emotion)
        # 各関節位置抽出
        centers = {}
        centers[0] = GET_ETC.get_centers(human_1, image_1)
        centers[1] = GET_ETC.get_centers(human_2, image_2)
        movement = GET_ETC.get_movement(centers[0], centers[1])

        # 体格抽出
        physique += GET_ETC.get_physique(centers[0], centers[1])
        # 抽出結果の受け渡し
        # 辞書生成
        dic = {"動き": movement, "年齢": age, "性別": gender, "怒り": emotion[0], "軽蔑": emotion[1], "嫌悪感": emotion[2],
               "恐怖": emotion[3], "喜び": emotion[4], "中立": emotion[5], "悲しみ": emotion[6], "驚き": emotion[7],
               "体格": physique}
        print(dic)
        # パーツ決定
        parts = DECIDE_PARTS.decide(dic)
        # RGB追加
        parts["R"] = bgr[2]
        parts["G"] = bgr[1]
        parts["B"] = bgr[0]

        return parts, dic
