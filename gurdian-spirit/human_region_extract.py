import argparse
import logging
import ast

from tf_openpose import common
import cv2
import numpy as np
from tf_openpose.estimator import TfPoseEstimator
from tf_openpose.networks import get_graph_path, model_wh

logger = logging.getLogger('human_region_extract')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def make_human_region(npimg, humans):
    """
    前景抽出のための前処理．
    推定された人物全ての前景を割り出す．割り出した結果は配列で出力される．
    各人物の順番は入力humansの結果に依存する．
    :param npimg: 推定に使った入力画像
    :param humans: ポーズ推定結果
    :return: list type, 人物の包含矩形，前景領域マスク，前景候補領域マスク
    """
    image_h, image_w = npimg.shape[:2]
    fgd_mask = np.full(npimg.shape[:2], 0, np.uint8)
    centers = []
    c_dic = {}
    rects = []
    fgd_masks = []
    pre_fgd_masks = []
    for human in humans:
        for i in range(common.CocoPart.Background.value):
            if i not in human.body_parts.keys():
                continue

            body_part = human.body_parts[i]
            center = (int(body_part.x * image_w + 0.5), int(body_part.y * image_h + 0.5))
            centers.append(center)
            c_dic[i] = center

        # スケールチェック
        shoulder_center = centers[1]
        waist_x = abs(centers[8][0] - centers[11][0])
        waist_y = abs(centers[8][1] - centers[11][1])
        waist_center = [(centers[8][0] + centers[11][0]) / 2, (centers[8][1] + centers[11][1]) / 2]
        upper_dis = np.sqrt(
            pow(shoulder_center[0] - waist_center[0], 2) + pow(shoulder_center[1] - waist_center[1], 2))

        # draw point
        for i in c_dic.keys():
            cv2.circle(fgd_mask, c_dic[i], int(upper_dis / 15), 255, thickness=-1, lineType=8, shift=0)

        # draw line
        # 仮調整　実際にとってみて値を変える必要あり？
        born_size = int(upper_dis / 5)

        for pair_order, pair in enumerate(common.CocoPairsRender):
            if pair[0] not in human.body_parts.keys() or pair[1] not in human.body_parts.keys():
                continue

            fgd_mask = cv2.line(fgd_mask, c_dic[pair[0]], c_dic[pair[1]], 255, born_size)
        ksize = born_size * 5
        pre_fgd_mask = cv2.blur(fgd_mask, ksize=(ksize, ksize))
        x, y, w, h = cv2.boundingRect(np.array(centers))
        margin = int(upper_dis)
        rects.append((x - margin, y - margin, w + 2 * margin, h + 2 * margin))
        fgd_masks.append(fgd_mask)
        pre_fgd_masks.append(pre_fgd_mask)
    return rects, fgd_masks, pre_fgd_masks


def extract_foreground_humans(image, fgd_masks, pre_fgd_masks):
    """
    前景抽出する．
    :param image: 推定に使った入力画像
    :param fgd_masks: 人物領域マスク
    :param pre_fgd_masks: 人物領域候補マスク
    :return: 前景マスク，前景画像，背景画像
    """
    bgdModel = np.zeros((1, 65), np.float64)
    fgdModel = np.zeros((1, 65), np.float64)
    final_mask = np.zeros(image.shape[:2], np.uint8)
    for mask, pre_mask in zip(fgd_masks, pre_fgd_masks):
        #    cv2.imshow('mask', mask)

        tmask = np.full(image.shape[:2], cv2.GC_BGD, np.uint8)
        tmask[pre_mask != 0] = cv2.GC_PR_FGD
        tmask[mask == 255] = cv2.GC_FGD
        cv2.grabCut(image, tmask, None, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_MASK)
        #    cv2.imshow('tmask', tmask * 80)
        pre_mask = np.where((tmask == 2) | (tmask == 0), 0, 1).astype(np.uint8)
        final_mask = np.logical_or(final_mask, pre_mask)
    fdg_image = image * final_mask[:, :, np.newaxis]
    bdg_image = image * np.logical_not(final_mask)[:, :, np.newaxis]

    return final_mask, fdg_image, bdg_image


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='tf-pose-estimation run')
    parser.add_argument('--image', type=str, default='./test.jpg')
    parser.add_argument('--resolution', type=str, default='432x368', help='network input resolution. default=432x368')
    parser.add_argument('--model', type=str, default='mobilenet_thin', help='cmu / mobilenet_thin')
    parser.add_argument('--scales', type=str, default='[None]', help='for multiple scales, eg. [1.0, (1.1, 0.05)]')
    args = parser.parse_args()
    scales = ast.literal_eval(args.scales)

    print(args.image)
    w, h = model_wh(args.resolution)
    e = TfPoseEstimator(get_graph_path(args.model), target_size=(w, h))

    # estimate human poses from a single image !
    image = common.read_imgfile(args.image, None, None)
    # image = cv2.fastNlMeansDenoisingColored(image, None, 10, 10, 7, 21)
    with common.Timer(logger, "inference image : {0}".format(args.image)):
        humans = e.inference(image, scales=scales)

    with common.Timer(logger, "人物領域抽出"):
        rects, fgd_masks, pre_masks = make_human_region(image, humans)

    with common.Timer(logger, "前景抽出"):
        final_mask, fg_img, bg_img = extract_foreground_humans(image, fgd_masks, pre_masks)


    # debug
    # image = TfPoseEstimator.draw_humans(image, humans, imgcopy=False)
    # cv2.imshow('tf-pose-estimation result', image)
    # cv2.imshow("fdg", fdg_image)
    # cv2.imshow("bdg", bdg_image)
    # cv2.waitKey(5)

    # Output iamges
    def combert2bgra(img, mask):
        b, g, r = cv2.split(img)
        alpha = mask.astype(np.uint8) * 255
        bgra = cv2.merge((b, g, r, alpha))
        return bgra


    import os

    fname = os.path.splitext(os.path.basename(args.image))[0]
    cv2.imwrite("{0}_{1}.png".format('fdg', fname), combert2bgra(fg_img, final_mask))
    cv2.imwrite("{0}_{1}.png".format('bdg', fname), combert2bgra(bg_img, np.logical_not(final_mask)))

    cv2.imwrite("mask_capture1.jpg", final_mask.astype(np.uint8) * 255)
    cv2.imwrite("capture1.jpg", image)

    cv2.destroyAllWindows()
    pass
